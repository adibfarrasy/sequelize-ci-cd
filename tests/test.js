const request = require("supertest");
const app = require("../index");
const { transaksi } = require("../models");

// Delete all data in transaksi
// beforeAll(async () => {
//   await transaksi.destroy({ where: {}, force: true });
// });

// Test
describe("Transaksi Test", () => {
  describe("/transaksi GET ALL", () => {
    it("It should get all transaksi data", async () => {
      const res = await request(app).get("/transaksi");

      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty("message");
      expect(res.body.message).toEqual("Success");
      expect(res.body).toHaveProperty("data");
    });
  });
});
